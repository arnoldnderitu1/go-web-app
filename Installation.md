## Installing Go
### Installing Go from source code
If you are familiar with Unix-like systems this would be your most preferable way of installing Go.
1. Install `gcc` or a similar compiler to compile Go. Using the `apt-get` package manager (included with Ubuntu) we can install the required compilers as follows:
```
sudo apt-get install gcc libc6-dev
```
2. Execute the following commands to clone the Go source code and compile it. (**This will clone the source code to your current directory.**)
```
git clone https://go.googlesource.com/go
cd go/src
./all.bash 
```
A successful installation will end with the message "ALL TESTS PASSED"
3.Next we need to set the environment variables. We can do so as follows:
```
export GOROOT=$HOME/go
export GOBIN=$GOROOT/bin
export PATH=$PATH:$GOROOT/bin
```
If you see the following information on your screen, you're all set.

![all set](1.1.mac.png)

## Install using third-party tools
### apt-get
Ubuntu is the most popular desktop release version of Linux. It uses `apt-get` to manage packages. We can install Go using the following commands
```
sudo add-apt-repository ppa:gophers/go
sudo apt-get update
sudo apt-get install golang-go
```

### wget
```
wget https://storage.googleapis.com/golang/go1.8.3.linux-amd64.tar.gz
sudo tar -xzf go1.8.3.linux-amd64.tar.gz -C /usr/local 

# Go environment
export GOROOT=/usr/local/go
export GOBIN=$GOROOT/bin
export PATH=$PATH:$GOBIN
export GOPATH=$HOME/gopath 
```

## Links
**Contents**: [Index](README.md)

**Previous Section**: [Welcome](README.md)

**Next Section**: [GOPATH and Workspace](GOPATH_and_Workspace.md)
