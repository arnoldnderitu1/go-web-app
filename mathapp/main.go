package main

import (
	"fmt"
	"go-web-app/mymath"
)

func main() {
	fmt.Printf("Hello, world. Sqrt(2) = %v\n", mymath.Sqrt(2))
}
