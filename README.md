# Welcome

The world of Go:
1. Fast project compilation
2. Garbage collection
3. Concurrent systems

## Index
- 1. GO Environment Configuration
    - 1.1. [Installing Go](Installation.md)
    - 1.2. [$GOPATH and Workspace](GOPATH_and_Workspace.md)
    - 1.3. [Go commands](GO_commands.md)
    - 1.4. [Go development tools](GO_development_tools.md)
- 2. GO Basic Knowledge
    - 2.1. [Hello GO](HELLO_GO.md)
    - 2.2. [GO foundation](GO_foundation.md)
- 3. Web Foundation
    - 3.1 [Build a simple web server](Simple_web_server.md)