# Go foundation
How do we define constants and variables in Go?

## Defining variables
The keyword `var` is the basic form of defining a variable, GO puts the variable type after the variable name

```
<!-- define a variable with name "variableName" and type "type" -->
var variableName type
```

Defining multiple variables
```
<!-- define three variables which types are "type" -->
var vname1, vname2, vname3 type
```

Define multiple variables with initial values
```
/*
    Define three variables with type "type", and initialize their values.
    vname1 is v1, vname2 is v2, vname3 is v3
*/
var vname1, vname2, vname3 type = v1, v2, v3
```
