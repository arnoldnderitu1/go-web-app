# Building a simple web server
Web applications are based on the HTTP protocol, and GO provides full HTTP support in the `net/http` package. It's very easy to set a web server up using this package.

## Using http package to setup a web server
```
package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
)

func sayhelloName(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()       // parse arguments, you have to call this by yourself
	fmt.Println(r.Form) // print form information in server side
	fmt.Println("path", r.URL.Path)
	fmt.Println("scheme", r.URL.Scheme)
	fmt.Println(r.Form["url_long"])
	for k, v := range r.Form {
		fmt.Println("key:", k)
		fmt.Println("val:", strings.Join(v, ""))
	}
	fmt.Fprintf(w, "Hello arnold!") // send data to client side
}

func main() {
	http.HandleFunc("/", sayhelloName)       // set router
	err := http.ListenAndServe(":9090", nil) // set listen port
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
```

Run the code above:
```
go run $GOPATH/src/go-web-app/server/main.go
```

Open your browser and visit `http://localhost:9090`. You'll see `Hello arnold` on your screen.

Try another address with additional arguments:
`http://localhost:9090/?url_long=111&url_long=222`