# Hello, Go
Let's learn how to write a simple program in GO. 

## Program
```
package main

import "fmt"

func main() {
    fmt.Printf("Hello, world or 你好，世界 or καλημ ́ρα κóσμ or こんにちは世界\n")
}
```
> [Hello world](hello_go/main.go)

The above program should print the following information:

```
Hello, world
```

## Explanation
GO programs are composed by `package`.

`package <pkgname>` (In this case `package main`) tells us this source file belongs to `main` package, and the keyword `main` tells us this package will be compiled to a program instead of package files whose extensions are `.a`

Every executable program has one and only one `main` package, and you need an entry function called `main` without any arguments or return values in the `main` package.

To print `Hello, world...`, we called a function called `Printf`. This function is coming from `fmt` package, so we import this package in the third line of source code, which is `import fmt`

On the 5th line we use the keyword `func` to deine the `main` function. The body of the function is inside of `{}`

On the 6th line, we called the function `Printf` which is from the package `fmt`. 

The example above contains many non-ASCII characters. The purpose is to demonstrate that Go supports UTF-8 by default. You can use any UTF-8 character in your programs.

## Links
* [Directory](README.md)

* **Previous section:** [GO development tools](GO_development_tools.md)

* **Next section:** [GO foundation](GO_foundation.md)

