// Example code for HELLO_GO chapter
// Purpose: Hellow world example demonstrating UTF-8 support
// To run in the console, type `go run main.go`
// If you see squares or question marks, you are missing language fonts

package main

import "fmt"

func main() {
	fmt.Printf("Hello, world or 你好，世界 or καλημ ́ρα κóσμ or こんにちは世界\n")
}
