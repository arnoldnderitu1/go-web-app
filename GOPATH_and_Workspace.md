# $GOPATH and Workspace
## $GOPATH
Go manages code files within a `$GOPATH` environment variable, a directory that contains all the Go code on the machine. 

The `$GOPATH` environment environment variable is different from the `$GOROOT` environment variable. The `$GOROOT` variable states where Go is installed on the machine.

To define the `$GOPATH` in *nix systems append the below export statement at the bottom of `.profile` file. 

> From Go 1.8, the `$GOPATH` variable has a default value. If not set: it defaults to `$HOME/go` on Unix.

```
nano ~/.profile
export GOPATH=${HOME}/mygo
. ~/.profile
```

> `~` is a shortcut to the current user's home directory.
. ~/.profile: to reload the profile file

To have more than one path (workspace) in `$GOPATH`

```
export GOPATH=${HOME}/mygo:${HOME}/Projects/go
```

> `:` separates the paths

It's highly recommended to not have multiple versions of `$GOPATH`

Create the following folders in `$GOPATH`:

* `src` for source files whose suffix is `.go`, `.c`, `.g`, `.s`
* `pkg` for compiled files whose suffix is `.a`.
* `bin` for executable files

## Package Directory
Create package source files and folders like:
`$GOPATH/src/mymath/sqrt.go`

> `mymath` is the package name

```
cd $GOPATH/src
mkdir mymath
cd mymath
touch sqrt.go
```

Every time you create a package, you should create a new folder in the `src` directory. The only exception is `main`. The `main` folder creation is optional. 

If you create a directory such as: `$GOPATH/src/github.com/arnoldnderitu1/beedb`, then the package path would be `github.com/arnoldnderitu1/beedb` and the package name will be the last directory in the path, which is `beedb` in this case.

Inside the `sqrt.go` file, type the following content

```
// Source code of $GOPATH/src/mymath/sqrt.go
package mymath

func Sqrt(x float64) float64 {
	z := 0.0
	for i := 0; i < 1000; i++ {
		z -= (z*z - x) / (2 * x)
	}
	return z
}
```

## Compile Packages
Now we need to compile the above package for practical purposes. There are two ways to do this:

1. Swtich your work path to the directory of your package, then execute the `go install` command.
2. Excecute the above command except with a file name, like `go install mymath`

After compiling, open the following folder:
```
cd $GOPATH/pkg/${GOOS}_${GOARCH}
// you can see the file was generated
mymath.a
```

The file whose suffix is `.a` is the binary file of our package. 

To use this file we will create a new application.

```
cd $GOPATH/src
mkdir mathapp
cd mathapp
touch main.go
```

Inside the `main.go` file, type the following code:
```
// $GOPATH/src/mathapp/main.go source code
package main

import (
	"mymath"
	"fmt"
)

func main() {
	fmt.Printf("Hello, world. Sqrt(2) = %v\n", mymath.Sqrt(2))
}
```
To compile the application navigate to the application directory, `$GOPATH/src/mathapp`, and execute 
```
go install
```
This will create an executable file called `mathapp` inside `$GOPATH/bin/`

To run the program, use the `./mathapp` command. 

```
$GOPATH/bin/mathapp
Hello world. Sqrt(2) = 1.414213562373095
```

## Install remote packages
Go has a tool for installing remote packages, which is a command called `go get`. 

You can use `go get -u ...` to update existing remote packages. This will automatically install all the dependent packages as well.

The tool uses different version control tools for different source platforms. E.g `git` for GitHub and `hg` for Google Code. 

Therefore, you have to install these version control tools before using `go get`

```
sudo apt install git-all
sudo apt-get install mercurial
```
To install a package from GitHub
```
go get github.com/astaxie/beedb
```
After executing the above command, the directory structure should look as below:
```
$GOPATH
	src
	 |-github.com
	 	 |-astaxie
	 	 	 |-beedb
	pkg
	 |--${GOOS}_${GOARCH}
	 	 |-github.com
	 	 	 |-astaxie
	 	 	 	 |-beedb.a
```
`go get` clones source code to the `$GOPATH/src` of the local file system, then executes `go install`.

To use remote packages:
```
import "github.com/astaxie/beedb"
```

# Directory Complete Structure
The complete directory structure should now look as below:

```
bin/
	mathapp
pkg/
	${GOOS}_${GOARCH}, such as darwin_amd64, linux_amd64
  mymath.a
  github.com/
    astaxie/
      beedb.a
src/
	mathapp
		main.go
	mymath/
		sqrt.go
	github.com/
		astaxie/
			beedb/
				beedb.go
				util.go
```

## Links
**Contents**: [Index](README.md)

**Previous Section**: [Installation](Installation.md)

**Next Section**: [GO Commands](GO_commands.md)
